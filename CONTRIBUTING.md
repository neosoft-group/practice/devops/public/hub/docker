# Contributing to Docker Hub Project

First off, thank you for considering contributing to the Docker Hub Project! It's people like you that make the open source community such a fantastic place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

## Getting Started

Before you begin:
- Have you read the code of conduct, and do you agree to follow it?
- Check out the issues or merge requests on our [GitLab project page](https://gitlab.com/neosoft-group/practice/devops/public/hub/docker) to see if someone already started working on your idea.
- If you are new to GitLab or open source, check out the [GitLab documentation](https://docs.gitlab.com/ee/gitlab-basics/README.html) and the [Open Source Guides](https://opensource.guide/) to get started!

## Your First Contribution

Unsure where to begin contributing to the Docker Hub Project? You can start by looking through issues labeled as `beginner` or `help wanted`:

- Beginner issues - issues which should only require a few lines of code, and a test or two.
- Help wanted issues - issues which should be a bit more involved than `beginner` issues.

Look for the labels in our [issue tracker](https://gitlab.com/neosoft-group/practice/devops/public/hub/docker/-/issues).

## Making Changes

1. Fork the repo and create your branch from `main`.
2. Make your changes. Ensure they are well tested and documented.
3. Commit your changes. Write a good commit message that explains the "why" of your changes.
4. Make sure your code lints.
5. Submit a merge request to the `main` branch. Use the merge request template provided by the repository.

## Using Merge Requests (MR)

After you've made your changes and you want to submit them for review:

- Open a new MR in GitLab.
- Ensure the MR description clearly describes the problem and solution. Include the relevant issue number if applicable.
- Link the MR to any corresponding issue(s).
- Before submitting, please read the contributions guide to know more about coding conventions and benchmarks.

## Code Review Process

The project maintainers look at Merge Requests on a regular basis. After feedback has been given we expect responses within two weeks. If feedback is not addressed within two weeks, the MR may be closed due to inactivity.

## Community

Stay up to date and get involved in discussions:
- [Project Issue Tracker](https://gitlab.com/neosoft-group/practice/devops/public/hub/docker/-/issues)

Don't hesitate to open new issues and add any suggestions or concerns you might have. We're all ears!

## Acknowledgements

Your contributions are under the MIT License. By contributing to the Docker Hub Project, you agree that your contributions will be licensed under its MIT License.

Thank you for your interest in contributing to the Docker Hub Project!
