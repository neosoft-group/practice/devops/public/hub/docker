/// <reference types='codeceptjs' />
type steps_file = typeof import('./steps_file');
type RunCommandHelper = import('./helpers/RunCommandHelper');

declare namespace CodeceptJS {
  interface SupportObject { I: I, current: any }
  interface Methods extends RunCommandHelper {}
  interface I extends ReturnType<steps_file>, WithTranslation<Methods> {}
  namespace Translation {
    interface Actions {}
  }
}
