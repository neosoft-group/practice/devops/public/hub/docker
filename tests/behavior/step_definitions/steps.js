const { I } = inject();

// A variable to store the execution result
let executionResult = '';

// Variable for workspace path
const workspacePath = '/workspace';

Given('a container named {string} is prepared based on the {string} image with Taskfile installed', (containerName, imageName) => {
  // Ensure container is running and ready
  I.runCommand(`docker rm -f ${containerName} || true`);
  I.runCommand(`docker run -d --name ${containerName} ${imageName} sleep infinity`);

  // Determine package manager based on imageName, install dependencies, and Taskfile
  const commandBase = `docker exec ${containerName} sh -c`;
  if (imageName.includes('debian') || imageName.includes('ubuntu')) {
    I.runCommand(`${commandBase} "apt-get update && apt-get install -y curl sudo"`);
  } else if (imageName.includes('fedora')) {
    I.runCommand(`${commandBase} "dnf install -y curl sudo"`);
  }

  I.runCommand(`${commandBase} "curl -sL https://taskfile.dev/install.sh | sh"`);
});

Given('the directory {string} has been copied into {string}', (filePath, containerName) => {
  I.runCommand(`docker cp ../../${filePath} ${containerName}:${workspacePath}`);
});

When('I execute the command {string} within {string}', async (taskName, containerName) => {
  try {
    executionResult = await I.runCommand(`docker exec ${containerName} bash -c "cd ${workspacePath} && ${taskName}"`, { encoding: 'utf8' });
  } catch (error) {
    executionResult = error.stdout || error.message;
    console.error(`Error whilst executing ${taskName} within the container ${containerName}: ${error.message}`);
  }
});

Then('the output should include {string}', (expectedMsg) => {
  if (!executionResult.includes(expectedMsg)) {
    throw new Error(`The anticipated message "${expectedMsg}" was not found within the execution result.`);
  }
});

Then('the output should not include {string}', (expectedMsg) => {
  if (executionResult.includes(expectedMsg)) {
    throw new Error(`The anticipated message "${expectedMsg}" was found within the execution result.`);
  }
});

Then('Docker should be verified as installed within {string}', async (containerName) => {
  const result = await I.runCommand(`docker exec ${containerName} docker --version`);
  if (!result.toString().includes('Docker version')) {
    throw new Error('Docker is not installed in the container ${containerName}');
  }
});

Given('I execute the command {string}', async (command) => {
  try {
    executionResult = await I.runCommand(`cd ../.. && ${command}`, { encoding: 'utf8' });
  } catch (error) {
    executionResult = error.stdout || error.message;
    console.error(`Error whilst executing "${command}": ${error.message}`);
  }
});

Given('the Docker environment is initialized and running', () => {
  I.runCommand(`docker --version`);
});
