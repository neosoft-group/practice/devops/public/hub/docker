import Helper from '@codeceptjs/helper';
import { execSync } from 'child_process';

class RunCommandHelper extends Helper {
    runCommand(command: string) {
        // console.debug(`Executing: ${command}`);
        try {
            const output = execSync(`${command}`, { stdio: 'pipe' });
            // console.debug(output.toString());
            return output.toString();
        } catch (error) {
            console.error(`Command failed: ${error}`);
            throw error;
        }
    }
}

export = RunCommandHelper;
