@system
Feature: Docker System Commands Verification
  In order to manage and maintain Docker environments effectively and efficiently
  As a developer or system administrator
  I want to ensure that 'docker system' commands execute as expected

  Background:
    Given the Docker environment is initialized and running

  @prune
  Scenario: Confirming execution of docker system prune
    Given I execute the command "task docker:system:prune"
    Then the output should include "Are you sure you want to continue? [y/N]"

  @prune_force
  Scenario: Executing docker system prune with force without confirmation
    Given I execute the command "task docker:system:prune DOCKER_SYSTEM_PRUNE_FORCE=true"
    Then the output should not include "Are you sure you want to continue? [y/N]"
    And the output should include "👍 Successfully pruned Docker system resources."