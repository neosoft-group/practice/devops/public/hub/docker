@install
Feature: Validate the 'install' Task Functionality
  In order to ensure reliable setup in containerized environments
  As a developer or system administrator
  I want to verify that the 'install' task completes successfully
  So that the necessary Docker and dependencies are correctly installed for development

  @linux
  Scenario Outline: Verifying 'install' task success across various Linux distributions
    Given a container named "<distro>_<version>" is prepared based on the "<distro>:<version>" image with Taskfile installed
    And the directory "template/.config/docker/" has been copied into "<distro>_<version>"
    When I execute the command "task install:<distro>" within "<distro>_<version>"
    Then the output should include "👍 Docker and dependencies are successfully installed."
    And Docker should be verified as installed within "<distro>_<version>"

    Examples: 
      | distro | version |
      | fedora | 39      |
      | ubuntu | 22.04   |
      | debian | 11      |
      | debian | 12      |
